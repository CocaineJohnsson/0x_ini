#ifndef H__INI_H
#define H__INI_H

#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include <ctype.h>
#include "0x_containers.h"

#define BUFFER_SIZE 512
typedef uint_fast32_t uint32;

/*do we error on newline or not, if so what's the retval?*/
#ifndef ERROR_NEWLINE
    #define ERROR_NEWLINE 0
#endif

static uint32 _readUntil(FILE *f, char *buffer, char d);

char* strip_leading(char *buffer)
{
    char buf2i[BUFFER_SIZE];
    char *buf2 = buf2i;
    strcpy(buf2, buffer);
    while(isspace((unsigned char)*buf2))
    {
        buf2++;
    }
    strcpy(buffer, buf2);
    return buffer;
}
char* strip_trailing(char *buffer)
{
    char *end;

    end = buffer + strlen(buffer) - 1;
    while(end > buffer && isspace((unsigned char)*end)) end--;

    end[1] = '\0';

    return buffer;
}

char* strip_blank(char *buffer)
{
    strip_leading(buffer);
    strip_trailing(buffer);
    return buffer;
}

uint32 parse_ini(FILE *f, uint32(*callback)
                (char *category, char *variable, char *value))
{
    if(f == NULL)
    {
        return 0xDEADDA7A;
    }

    if(callback == NULL)
    {
        return ;
    }

    char category[BUFFER_SIZE] = { '\0' };
    char variable[BUFFER_SIZE] = { '\0' };
    char value[BUFFER_SIZE]    = { '\0' };
    char buffer[BUFFER_SIZE]   = { '\0' };

    uint32 c = 0x0, eofr = 0x0;

    while(!eofr)
    { 
        c = getc(f);
        switch (c)
        {
            case ';': case '#':
                ungetc(c, f);
                _readUntil(f, variable, '\n');
                break;
            case '[':
                memset(category, 0, BUFFER_SIZE);
                _readUntil(f, category, ']');
                break;
                
            case '=':
                _readUntil(f, value, '\n');
                break;
                
            case '\n': case EOF:
                if(strcmp(value, "\0") == 0)
                    strcpy(value, "\'\'");
                if(c == EOF)
                    eofr = 1;
                strcpy(buffer, variable);
                if(strcmp(strip_blank(buffer), "]") == 0)
                {
                    memset(variable, 0, BUFFER_SIZE);
                    memset(value, 0, BUFFER_SIZE);
                    break;
                }
                callback(category, variable, value);
                memset(variable, 0, BUFFER_SIZE);
                memset(value, 0, BUFFER_SIZE);
                break;
                
            default:
                ungetc(c, f);
                _readUntil(f, variable, '\n');
                break;
        }
    }
    return EXIT_SUCCESS;
}

/*
 * Wipes the entire file and dumps all the data given sequentially. 
 *
 * Usage: pass the path, followed by an ini_t struct. 
 * This function returns a nonzero error code on failure.
 */
uint32 dump_ini(const char *path, ini_t *ini)
{
    FILE *f;
    
    if(ini == NULL)
       return  0xDEADDA7A;
    
    f = fopen(path, "w+");
    if(f == NULL)
        return 0xDEADDA7A;
    
    while(ini->prev != NULL)
        ini=ini->prev;
    
    char *buf = "temporary buffer value";
    char tmp[BUFFER_SIZE*3];
    
    ini->variable = ini->varHead;
    ini->value    = ini->valHead;
    
    while(ini != NULL)
    {

        if(strcmp(buf, ini->category) != 0)
        {
            buf = ini->category;
            if(strcmp(buf, "\0"))
            {
                strcpy(tmp, "[");
                strcat(tmp, buf);
                strcat(tmp, "]\n");
                fputs( tmp, f);   
            }
            
            strcpy(tmp, "\0");
            ini->value    = ini->valHead;
            ini->variable = ini->varHead;
        }
   
        while(ini->variable != NULL)
        {
            strcpy(tmp, ini->variable->entry);
            if(ini->value != NULL && strcmp(ini->value->entry, "\'\'") != 0)
            {
                /*
                 * we probably want to strip the first trailing space of the variable
                 * and the first leading space of the value to prevent clobbering with new spaces
                 * this is only a 'bug' if you do not strip whitespace already, if you do
                 * then this does not apply. Tabs might look nicer than spaces, this is up to you.
                 */
                strcat(tmp, " = ");
                strcat(tmp, ini->value->entry);
            }

            ini->variable = ini->variable->next;
            if(ini->value != NULL)
                ini->value=ini->value->next;
               
            strcat(tmp, "\n");
            fputs(tmp, f);
            strcpy(tmp, "\0");
        }
   
        memset(tmp, 0, sizeof(tmp));
        ini=ini->next;
    }
    
    fclose(f);
    return EXIT_SUCCESS;
}

static uint32 _readUntil(FILE *f, char *buffer, char d)
{
    char c;
    uint32 i = 0, string = 0;
    while (i < BUFFER_SIZE) 
    {
        c = getc(f);
     
        if(c == '\"')
            string ^= 1;
        if(string == 0)
        {
            if (c == ']' && c == d)
            {
                ungetc(c, f);
                buffer[i] = '\0';
                return 0;
            }
            else if (c == '=' || c == '\n' || c == '[')
            {
                ungetc(c, f);
                buffer[i] = '\0';
                return 0;
            }
            else if (c == EOF) 
            {   
                buffer[i] = '\0';
                return 0;
            }
        }
        buffer[i++] = c;
    }
    buffer[i] = '\0';
    return EXIT_SUCCESS;
}

#endif /*H__INI_H*/
